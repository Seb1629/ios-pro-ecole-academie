//
//  ModelInAppPurchase.swift
//  iOSProEcoleAcademie
//
//  Created by SEBASTIEN MONTIBELLER on 6/4/17.
//  Copyright © 2017 SEBASTIEN MONTIBELLER. All rights reserved.
//

import Foundation

struct ModelInAppPurchase {
  
  var name: String!
  let productIdentifier: String!
  let price: Double
  let points: Int
  
}
