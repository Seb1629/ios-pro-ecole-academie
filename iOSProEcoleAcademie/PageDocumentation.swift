//
//  PageDocumentation.swift
//  iOSProEcoleAcademie
//
//  Created by SEBASTIEN MONTIBELLER on 6/4/17.
//  Copyright © 2017 SEBASTIEN MONTIBELLER. All rights reserved.
//

import Foundation

struct PageDocumentation {
  
  let number: Int!
  let slideImage: String!
  let reference: String!
  
}
