//
//  CreateTutorialViewController.swift
//  iOSProEcoleAcademie
//
//  Created by SEBASTIEN MONTIBELLER on 21/4/17.
//  Copyright © 2017 SEBASTIEN MONTIBELLER. All rights reserved.
//

import UIKit

class CreateTutorialViewController: UITableViewController  {

  @IBOutlet weak var tutorialUIDTextfield: UITextField!
  @IBOutlet weak var headerTextView: UITextView!
  @IBOutlet weak var tutorialDescTextView: UITextView!
  @IBOutlet weak var numberOfEpisodeTextField: UITextField!
  @IBOutlet weak var pricePointsTextfield: UITextField!
  @IBOutlet weak var tutorialImageView: UIImageView!
  @IBOutlet weak var previewVideoUrl: UITextField!
  
  var tutorialImage: UIImage!
  
    override func viewDidLoad() {
        super.viewDidLoad()
      tableView.delegate = self
      tableView.dataSource = self

    }


  @IBAction func saveClicked(_ sender: Any) {
    guard let tutorialId = tutorialUIDTextfield.text, let header = headerTextView.text, let tutorialDesc = tutorialDescTextView.text, let numberOfEpisode = numberOfEpisodeTextField.text, let pricePoint = pricePointsTextfield.text, let previewUrl = previewVideoUrl.text else {
      
      // TODO:- display alert so I know it has not been saved
      return
    }
    
    let tutorial = Tutorial(id: tutorialId, header: header, tutorialDescription: tutorialDesc, pricePoints: Int(pricePoint)!, previewUrl: previewUrl, numberOfEpisode: Int(numberOfEpisode)!)
  
    tutorial.saveTutorial { (error) in
      if let error = error {
        print(error.localizedDescription)
        return
      }
      print("saved successfully to firebase")
      self.dismiss(animated: true, completion: nil)
      
    }
  }

  
  @IBAction func cancelClicked(_ sender: Any) {
    self.dismiss(animated: true, completion: nil)
  }

}


