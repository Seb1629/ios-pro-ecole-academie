//
//  DocumentationPageViewController.swift
//  iOSProEcoleAcademie
//
//  Created by SEBASTIEN MONTIBELLER on 5/4/17.
//  Copyright © 2017 SEBASTIEN MONTIBELLER. All rights reserved.
//

import UIKit

class DocumentationPageViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

  struct CellIds {
    static let reuseIdentifier = "reuseIdentifier"
    
  }

  var lesson: Lesson?
  let pages = [Tutorial]()
  
  
    override func viewDidLoad() {
        super.viewDidLoad()
      AppUtility.lockOrientation(.portrait)
      configureCollectionView()
      self.title = "lesson 1"
      self.navigationItem.leftBarButtonItem =  UIBarButtonItem(title: "Close", style: .plain, target: self, action: #selector(handleClosePressed))
      self.navigationItem.leftBarButtonItem?.tintColor = UIColor.black
    }

  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

  }
  
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    AppUtility.lockOrientation(.portrait)
  }
  
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    AppUtility.lockOrientation(.all)
  }
  
  
  func configureCollectionView() {
    collectionView?.register(DocumentationCell.self, forCellWithReuseIdentifier: CellIds.reuseIdentifier)
    let layout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout
    layout?.scrollDirection = .horizontal
    layout?.minimumLineSpacing = 0
    collectionView?.isPagingEnabled = true
    collectionView?.showsHorizontalScrollIndicator = false
    collectionView?.backgroundColor = .white
      }
  
  
  func handleClosePressed() {
    self.dismiss(animated: true, completion: nil)
  }

}


  
extension DocumentationPageViewController {
  
  override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return 1
  }
  
  override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIds.reuseIdentifier, for: indexPath) as! DocumentationCell
    
//    let currentPage = pages[indexPath.item]
//    cell.pages = currentPage
    return cell
  }

  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width: self.view.frame.width, height: self.view.frame.height - 64)
  }
  
}


class DocumentationCell: UICollectionViewCell {
  
  var pages: Tutorial? {
    didSet {
      
    }
  }
  
  let bottomView: UIView = {
    let view = UIView()
    view.backgroundColor = .lightGray
    view.translatesAutoresizingMaskIntoConstraints = false
    return view
  }()
  
  let pageView: UIImageView = {
    let iv = UIImageView()
    iv.contentMode = .scaleAspectFill
    iv.image = #imageLiteral(resourceName: "lessonTest")
    return iv
  }()
  
  let pageCount: UILabel = {
    let label = UILabel()
    label.text = "4/6"
    label.font = UIFont.systemFont(ofSize: 14)
    label.translatesAutoresizingMaskIntoConstraints = false
    label.textAlignment = .right
    return label
  }()
  
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setupConstraint()
  }
  
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
 
  func setupConstraint() {
    addSubview(bottomView)
    addSubview(pageView)
    bottomView.addSubview(pageCount)
    bottomView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
    bottomView.heightAnchor.constraint(equalToConstant: 32).isActive = true
    bottomView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
    bottomView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
    
    pageView.anchorWithConstantsToTop(self.topAnchor, left: self.leftAnchor, bottom: bottomView.topAnchor, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
    
    pageCount.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -20).isActive = true
    pageCount.centerYAnchor.constraint(equalTo: bottomView.centerYAnchor).isActive = true
  }
  
  
  
  
  
  
  
  
}
