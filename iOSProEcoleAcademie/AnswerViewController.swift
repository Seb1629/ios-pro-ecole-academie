//
//  AnswerViewController.swift
//  iOSProEcoleAcademie
//
//  Created by SEBASTIEN MONTIBELLER on 17/4/17.
//  Copyright © 2017 SEBASTIEN MONTIBELLER. All rights reserved.
//

import UIKit

class AnswerViewController: UIViewController {

  @IBOutlet weak var questionLabel: UILabel!
  @IBOutlet weak var answerImageView: UIImageView!
  
  
  
  var question: CommonQuestion!
  
    override func viewDidLoad() {
        super.viewDidLoad()
      questionLabel.text = question.question
      
    }

  @IBAction func returnTapped(_ sender: Any) {
    self.dismiss(animated: true, completion: nil)
  }
  

}
