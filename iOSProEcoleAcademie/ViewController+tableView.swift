//
//  ViewController+tableView.swift
//  iOSProEcoleAcademie
//
//  Created by SEBASTIEN MONTIBELLER on 8/4/17.
//  Copyright © 2017 SEBASTIEN MONTIBELLER. All rights reserved.
//

import UIKit

// MARK: - CollectionView Datasource And Delegate

extension ViewController: UITableViewDataSource, UITableViewDelegate {
  
  
  func configCollectionView() {
    tableView.delegate = self
    tableView.dataSource = self
    tableView.estimatedRowHeight = Storyboard.appCellHeight
    tableView.rowHeight = UITableViewAutomaticDimension
    tableView.separatorColor = .clear
    tableView.estimatedSectionHeaderHeight = Storyboard.cellHeaderHeight
    tableView.sectionHeaderHeight = UITableViewAutomaticDimension
   
  }
  
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if tutorials.count > 0 {
      return 1
    }
    
    return 0
  }
  
  
  func numberOfSections(in tableView: UITableView) -> Int {
    
    return tutorials.count
  }
  
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: Storyboard.cellId, for: indexPath) as! AppCell
    
    let tutorial = tutorials[indexPath.section]
    cell.delegate = self
    cell.user = user
    cell.tutorial = tutorial
    return cell
  }
  
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let cell = tableView.dequeueReusableCell(withIdentifier: Storyboard.headerCellId) as! HeaderCell
    
    let headerTutorial = tutorials[section]
    
    cell.concept = headerTutorial
    
    return cell
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return UITableViewAutomaticDimension
  }
  
  
  // to hide per section on a button click
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//    if indexPath.section == 0 {
//      return 0
//    }
    
    return UITableViewAutomaticDimension
}
}
