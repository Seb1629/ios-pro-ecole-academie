//
//  Lessons.swift
//  iOSProEcoleAcademie
//
//  Created by SEBASTIEN MONTIBELLER on 16/4/17.
//  Copyright © 2017 SEBASTIEN MONTIBELLER. All rights reserved.
//

import UIKit
import CloudKit

class Lesson {
  
  let id: String
  let lessonNumberForImage: Int
  var tutorialImage: UIImage?
  let tutorialDescription: String
  let tutorialUrl: String
  let tutorialTitle: String
  
  
  
  init(id: String, tutorialDescription: String, tutorialUrl: String, lessonNumberForImage: Int, lessonTitle: String) {
//    self.tutorialImage = tutorialImage
    self.tutorialDescription = tutorialDescription
    self.tutorialUrl = tutorialUrl
    self.id = id
    self.lessonNumberForImage = lessonNumberForImage
    self.tutorialTitle = lessonTitle
  }
  
  
  init(dictionary: [String: Any]) {
    self.tutorialDescription = dictionary["tutorialDescription"] as? String ?? ""
    self.tutorialUrl = dictionary["tutorialUrl"] as? String ?? ""
    self.tutorialImage = dictionary["tutorialImage"] as? UIImage
    self.id = dictionary["id"] as! String
    self.lessonNumberForImage = dictionary["lessonNumberForImage"] as? Int ?? 0
    self.tutorialTitle = dictionary["lessonTitle"] as? String ?? ""
  }
  
  
  func saveTutorial(completion: @escaping (Error?) -> Void) {
    let ref = DatabaseReference.tutorialLessons.reference().child(id).childByAutoId()
    ref.setValue(toDictionary())
    
    // FIXME:- Might do this process manually from storage
    
    //4 - save the profile image
//    if let tutorialImage = self.tutorialImage {
//      let firImage = FIRImage(image: tutorialImage)
//      
//      firImage.saveLessonImage(self.id, self.lessonNumberForImage, { (error) in
//        completion(error)
//      })
//    }
  }

  
  func toDictionary() -> [String: Any] {
    return [
      "id" : id,
      "tutorialDescription" : tutorialDescription,
      "tutorialUrl" : tutorialUrl,
      "lessonNumberForImage" : lessonNumberForImage,
      "lessonTitle" : tutorialTitle
    ]
  }
  
  
  func downloadLessonImage(completion: @escaping (UIImage?, NSError?) -> Void) {
    FIRImage.downloadLessonImage(uid: id, lessonForImage: lessonNumberForImage) { (image, error) in
      self.tutorialImage = image
      completion(image, error as NSError?)
    }
  }
  
}


extension Lesson {
  
 class func fetchLessonsFromDatabase(tutorialId: String, completion: @escaping (_ lesson: Lesson?) -> Void) {
    
    DatabaseReference.tutorialLessons.reference().child(tutorialId).observe(.childAdded, with: { (snapshot) in
      if let dict = snapshot.value as? [String: Any] {
      
          let lesson = Lesson(dictionary: dict)
          completion(lesson)
        
      }
    }, withCancel: nil)
  }
  
}

 
