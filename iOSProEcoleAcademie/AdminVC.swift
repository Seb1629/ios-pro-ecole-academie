//
//  AdminVC.swift
//  iOSProEcoleAcademie
//
//  Created by SEBASTIEN MONTIBELLER on 21/4/17.
//  Copyright © 2017 SEBASTIEN MONTIBELLER. All rights reserved.
//

import UIKit

class AdminVC: UIViewController {

  struct Storyboard {
  static let goToTutorialScreen = "goToTutorialScreen"
  static let goToLesson = "goToLesson"
  }
  
  
    override func viewDidLoad() {
        super.viewDidLoad()

    }

  
  @IBAction func cancelTapped(_ sender: Any) {
    self.dismiss(animated: true, completion: nil)
  }

  
  @IBAction func tutorialPressed(_ sender: UIButton) {
    switch sender.tag {
    case 0:
      performSegue(withIdentifier: Storyboard.goToTutorialScreen, sender: nil)
    case 1:
      performSegue(withIdentifier: Storyboard.goToLesson, sender: nil)
    default:
      print("not me")
    }
  }

}
