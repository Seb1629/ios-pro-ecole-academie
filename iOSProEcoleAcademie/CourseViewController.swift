//
//  CourseViewController.swift
//  iOSProEcoleAcademie
//
//  Created by SEBASTIEN MONTIBELLER on 16/4/17.
//  Copyright © 2017 SEBASTIEN MONTIBELLER. All rights reserved.
//

import UIKit
import CloudKit
import MessageUI
import SwiftSpinner

class CourseViewController: UIViewController, MFMailComposeViewControllerDelegate {

  struct Storyboard {
    static let courseCell = "courseCell"
  }
  
  struct StoryBoardSegue {
    static let showQuestions = "showQuestions"
    static let goToWebview = "goToWebview"
    static let goToDocumentation = "goToDocumentation"
  }
  
  // Properties
  var tutorial: Tutorial?
  var lessons = [Lesson]()
  var lessonPassed: Lesson?
  var currentLessonIndexPath: Int?
  var currentUser: User!
  
  // Outlets
  @IBOutlet weak var collectionView: UICollectionView!
  @IBOutlet weak var numberOfParts: UILabel!
  @IBOutlet weak var pageControl: UIPageControl!
  
  
  // Initializers
    override func viewDidLoad() {
        super.viewDidLoad()
      collectionView.delegate = self
      collectionView.dataSource = self
      fetch()
    }

  
  func fetch() {

    guard let tutorial = tutorial else { return }
    
    Lesson.fetchLessonsFromDatabase(tutorialId: tutorial.id, completion: { [weak self] (lesson) in
      
      if let lesson = lesson {
        DispatchQueue.main.async {
          self?.lessons.append(lesson)
          self?.collectionView.reloadData()
          if let strongSelf = self {
            strongSelf.numberOfParts.text = "Partie 1 / \(strongSelf.lessons.count)"
          }
        }
      }
  })
}
  
  
  // Methods
  @IBAction func returnButtonTapped(_ sender: Any) {
    self.dismiss(animated: true, completion: nil)
  }

  
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
      
      if segue.identifier == StoryBoardSegue.showQuestions {
        if let destination = segue.destination as? QCMViewController {
          if let sender = sender as? Lesson {
            destination.lesson = sender
            
          }
        }
      } else if segue.identifier == StoryBoardSegue.goToWebview {
        if let destination = segue.destination as? WebViewController {
          if let sender = sender as? Lesson {
            destination.lesson = sender
          }
        }
      } else if segue.identifier == StoryBoardSegue.goToDocumentation {
        if let destination = segue.destination as? DocumentationPageViewController {
          if let sender = sender as? Lesson {
            destination.lesson = sender
          }
        }
      }
    }


  func dismissAndRunVideoPlayer(lesson: Lesson) {
    performSegue(withIdentifier: StoryBoardSegue.goToWebview, sender: lesson)
  }
  
  func showSendMailErrorAlert() {
    let sendMailErrorAlert = UIAlertController(title: "Could not send email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", preferredStyle: .alert)
    let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
    sendMailErrorAlert.addAction(okAction)
    present(sendMailErrorAlert, animated: true, completion: nil)
  }
  
  
  func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
    controller.dismiss(animated: true, completion: nil)
  }
  
  
  func configureMailToSendXcodeProjectComposeViewController(lesson: Lesson) -> MFMailComposeViewController {
    
    let mailComposer = MFMailComposeViewController()
    mailComposer.mailComposeDelegate = self
    mailComposer.setToRecipients([currentUser.emailAddress])
    mailComposer.setSubject("Telecharge le projet")
    mailComposer.setMessageBody("Envoie le projet dans ma boite email et clique le lien pour commencer le telechargement. \n Clique ce lien: https://www.apple.com", isHTML: false)
    
    return mailComposer
  }
  
  
  func dismissAndDownloadMaterial(lesson: Lesson) {
    let mailComposeViewController = configureMailToSendXcodeProjectComposeViewController(lesson: lesson)
    if MFMailComposeViewController.canSendMail() {
      self.present(mailComposeViewController, animated: true, completion: nil)
      
    } else {
      showSendMailErrorAlert()
    }
  }
  
  
  func openDocumentationViewer(lesson: Lesson) {
    let layout = UICollectionViewFlowLayout()
    let docPageController = DocumentationPageViewController(collectionViewLayout: layout)
    let navController = UINavigationController(rootViewController: docPageController)
    present(navController, animated: true, completion: nil)
  }
  
  
  func dismissAndShowMostCommonQuestionsAndAnswers(lesson: Lesson) {
    performSegue(withIdentifier: StoryBoardSegue.showQuestions, sender: lesson)
  }
}


// MARK:- DATASOURCE AND DELEGATE
extension CourseViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
  
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    pageControl.numberOfPages = lessons.count
    return lessons.count
  }
  
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Storyboard.courseCell, for: indexPath) as? LearnCell {
      cell.delegate = self
      let currentLesson = lessons[indexPath.item]
      cell.lesson = currentLesson
      
      return cell
    } else {
      return UICollectionViewCell()
    }
  }
  
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
  }
  
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return 0
  }
  
  
  func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
    let pageNumber = Int(targetContentOffset.pointee.x / self.collectionView.frame.width)
    pageControl.currentPage = pageNumber
    numberOfParts.text = "Partie \(pageNumber + 1) / \(lessons.count)"
  }
}


extension CourseViewController {
  
  func showActionSheet(lesson: Lesson) {
    let alert = UIAlertController(title: "Choisi une action", message: "", preferredStyle: .actionSheet)
    alert.addAction(UIAlertAction(title: "Regarde video lesson", style: .default, handler: { (action) in
      self.dismissAndRunVideoPlayer(lesson: lesson)
    }))
    
    alert.addAction(UIAlertAction(title: "Telecharge Xcode Project", style: .default, handler: { (action) in
      self.dismissAndDownloadMaterial(lesson: lesson)
    }))
    
    alert.addAction(UIAlertAction(title: "Documentation", style: .default, handler: { (action) in
      self.openDocumentationViewer(lesson: lesson)
    }))
    
    alert.addAction(UIAlertAction(title: "Questions - Reponses", style: .default, handler: { (action) in
      self.dismissAndShowMostCommonQuestionsAndAnswers(lesson: lesson)
    }))
    
    alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
    
    self.present(alert, animated: true, completion: nil)
  }
}


extension CourseViewController: MyCellDelegate {
  
  func lessonSelectedIndexPath(lesson: Lesson) {
//    self.lessonPassed = cell.lesson
    showActionSheet(lesson: lesson)
    print("lesson: \(lesson.lessonNumberForImage)")
  }
  
}



