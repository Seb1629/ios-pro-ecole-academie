//
//  LearnCell.swift
//  iOSProEcoleAcademie
//
//  Created by SEBASTIEN MONTIBELLER on 16/4/17.
//  Copyright © 2017 SEBASTIEN MONTIBELLER. All rights reserved.
//

import UIKit
import SAMCache


protocol MyCellDelegate: class {
  
  func lessonSelectedIndexPath(lesson: Lesson)
  
}

class LearnCell: UICollectionViewCell {
    
  @IBOutlet weak var courseImage: UIImageView!
  @IBOutlet weak var descTextview: UITextView!
  @IBOutlet weak var titleLabel: UILabel!
  
  weak var delegate: MyCellDelegate? = nil
  var cache = SAMCache.shared()
  
  var lesson: Lesson? {
    didSet {
      updateUI()
 
    }
  }
  
  func updateUI() {
    
    guard let lesson = lesson else { return }
    
    titleLabel.text = lesson.tutorialTitle
    descTextview.scrollRangeToVisible(NSMakeRange(0, 0))
    descTextview.text = lesson.tutorialDescription
    
    // image
    
    let lessonImageKey = "\(lesson.id)\(lesson.lessonNumberForImage)-lessonImage"

    if let image = cache?.object(forKey: lessonImageKey) as? UIImage {
      courseImage.image = image

    } else {
      
      lesson.downloadLessonImage { [weak self] (image, error) in
        self?.courseImage.image = image
        self?.courseImage.contentMode = .scaleAspectFit
        self?.courseImage.clipsToBounds = true
        self?.cache?.setObject(image, forKey: lessonImageKey)

      }
    }
  }
  
  
  @IBAction func apprendPressed(_ sender: Any) {
    if let lesson = lesson {
      delegate?.lessonSelectedIndexPath(lesson: lesson)

    }
  }

  
}
