//
//  Constants.swift
//  iOSProEcoleAcademie
//
//  Created by SEBASTIEN MONTIBELLER on 8/4/17.
//  Copyright © 2017 SEBASTIEN MONTIBELLER. All rights reserved.
//

import Foundation

struct UserDefaultsKeys {
    static let emailAddress = "emailAddress"
    static let points = "points"
    static let tutorialUnlocked = "tutorialUnlocked"
    static let name = "name"
    static let uid = "uid"
    static let userObject = "userObject"
}

public var userForkey = ""


struct NotificationName {
  static let callPurchasePointButton = Notification.Name("callPurchasePointButton")
}
