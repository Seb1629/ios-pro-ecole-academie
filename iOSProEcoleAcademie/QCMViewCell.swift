//
//  QCMViewCell.swift
//  iOSProEcoleAcademie
//
//  Created by SEBASTIEN MONTIBELLER on 5/4/17.
//  Copyright © 2017 SEBASTIEN MONTIBELLER. All rights reserved.
//

import UIKit

class QCMViewCell: UITableViewCell {

  
  @IBOutlet weak var questionLabel: UILabel!
  
  
  var question: CommonQuestion! {
    didSet {
      questionLabel.text = question.question
      questionLabel.numberOfLines = 0
    }
  }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
