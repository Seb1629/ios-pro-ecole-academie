//
//  EmptyView.swift
//  iOSProEcoleAcademie
//
//  Created by SEBASTIEN MONTIBELLER on 17/4/17.
//  Copyright © 2017 SEBASTIEN MONTIBELLER. All rights reserved.
//

import UIKit

class EmptyView {
  
  lazy var emptyView = UIView()
  var view: UIView
  
  
  let messageLabel: UILabel = {
    let label = UILabel()
    var string1 = "On a pas encore recu de questions a propos de ce topic. \n\n"
    let attributes = [NSFontAttributeName: UIFont.boldSystemFont(ofSize: 16)]
    let string2 = "Envoyez nous vos questions et nous posterons les reponses sur cette page"
    
    let attributedString = NSAttributedString(string: string1.appending(string2), attributes: attributes)
    
    label.attributedText = attributedString
    label.numberOfLines = 0
    label.translatesAutoresizingMaskIntoConstraints = false
    label.textAlignment = .center
    label.sizeToFit()
    return label
  }()
  
  
  init(view: UIView) {
    self.view = view
  }
  
  func displayView() {
    
    emptyView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height - 60)
    view.addSubview(emptyView)
    emptyView.backgroundColor = .white
    emptyView.addSubview(messageLabel)
    
   let _ = messageLabel.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: 0)
        
  }
  
}
