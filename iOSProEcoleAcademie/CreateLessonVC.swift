//
//  CreateLessonVC.swift
//  iOSProEcoleAcademie
//
//  Created by SEBASTIEN MONTIBELLER on 25/4/17.
//  Copyright © 2017 SEBASTIEN MONTIBELLER. All rights reserved.
//

import UIKit

class CreateLessonVC: UITableViewController {

  @IBOutlet weak var lessonUIDTextfield: UITextField!
  @IBOutlet weak var tutorialDescriptionTextview: UITextView!
  @IBOutlet weak var previewUrlTextfield: UITextField!
  @IBOutlet weak var imageLesson: UIImageView!
  @IBOutlet weak var lessonTitle: UITextField!
  
  
  
  @IBOutlet weak var episodeNumber: UITextField!
  var tutorialImage: UIImage!
  var imagePickerHelper: ImagePickerHelper!
  
    override func viewDidLoad() {
        super.viewDidLoad()
      tableView.delegate = self
      tableView.dataSource = self
    }


  @IBAction func savePressed(_ sender: Any) {
    
    guard let lessonId = lessonUIDTextfield.text, let tutorialDesc = tutorialDescriptionTextview.text, let lessonUrl = previewUrlTextfield.text, let episodeNumber = episodeNumber.text, let lessonTitle = lessonTitle.text else {
      
      // display alert so I know it has not been saved
      return
    }
    
  
    let lesson = Lesson(id: lessonId, tutorialDescription: tutorialDesc, tutorialUrl: lessonUrl, lessonNumberForImage: Int(episodeNumber)!, lessonTitle: lessonTitle)
    
    lesson.saveTutorial { (error) in
      if let error = error {
        print(error.localizedDescription)
        return
      }
      print("lesson saved successfully to firebase")
    }
 
  }
  
  @IBAction func cancelPressed(_ sender: Any) {
    self.dismiss(animated: true, completion: nil)
  }
  
}
