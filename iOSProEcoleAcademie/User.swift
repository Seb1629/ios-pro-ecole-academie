//
//  User.swift
//  iOSProEcoleAcademie
//
//  Created by SEBASTIEN MONTIBELLER on 7/4/17.
//  Copyright © 2017 SEBASTIEN MONTIBELLER. All rights reserved.
//

import UIKit
import CloudKit
import Firebase


class User: NSObject, NSCoding {
  
  var uid: String
  var name: String
  var emailAddress: String
  var points: Int
  var tutorialUnlocked = [String]()
  
   init(uid: String, emailAddress: String, name: String) {
    self.uid = uid
    self.emailAddress = emailAddress
    points = 100
    self.name = name
    tutorialUnlocked = []
  }
  
  
  init?(dictionary: [String: Any]) {
  
    guard let uid = dictionary["userId"] as? String, let points = dictionary["points"] as? Int else {
      return nil
    }
    
    self.uid = uid
    self.name = dictionary["name"] as? String ?? ""
    self.emailAddress = dictionary["emailAddress"] as? String ?? ""
    self.points = points
    // tutorial unlocked
  }
  
  
  func encode(with aCoder: NSCoder) {
    aCoder.encode(self.emailAddress, forKey: UserDefaultsKeys.emailAddress)
    aCoder.encode(self.points, forKey: UserDefaultsKeys.points)
    aCoder.encode(self.tutorialUnlocked, forKey: UserDefaultsKeys.tutorialUnlocked)
    aCoder.encode(self.uid, forKey: UserDefaultsKeys.uid)
    aCoder.encode(self.name, forKey: UserDefaultsKeys.name)

  }
  
  required init?(coder aDecoder: NSCoder) {
    self.emailAddress = aDecoder.decodeObject(forKey: UserDefaultsKeys.emailAddress) as? String ?? ""
    self.points = aDecoder.decodeInteger(forKey: UserDefaultsKeys.points)
    self.tutorialUnlocked = aDecoder.decodeObject(forKey: UserDefaultsKeys.tutorialUnlocked) as? [String] ?? []
    self.uid = aDecoder.decodeObject(forKey: UserDefaultsKeys.uid) as? String ?? ""
    self.name = aDecoder.decodeObject(forKey: UserDefaultsKeys.name) as? String ?? ""

  }
  
  
  func insertItemsToUserDefault() {
    let data = NSKeyedArchiver.archivedData(withRootObject: self)
    UserDefaults.standard.set(data, forKey: UserDefaultsKeys.userObject)
    UserDefaults.standard.synchronize()
  }
  
}


// method for firebase
extension User {
  
  func save(completion: @escaping (Error?) -> Void) {
    let ref = DatabaseReference.users(uid: uid).reference()
    ref.setValue(toDictionary())
  }
  
  
  func toDictionary() -> [String: Any] {
    return [
      "userId" : uid,
      "name" : name,
      "emailAddress" : emailAddress,
      "points" : points,
      "tutorialUnlocked" : tutorialUnlocked
      
    ]
  }

  
}



extension User {
  
  func purchaseItemForNumberOf(tutorial: Tutorial, viewController: UIViewController, button: UIButton, completion: @escaping () -> Void) {
        if self.points >= tutorial.pricePoints {
          
          showAlertConfirmPurchase(viewController: viewController, tutorial: tutorial, button: button, completion: completion)
          
        } else {
          
          let alert = UIAlertController(title: "Ooops tu n'as pas assez de points", message: "Veux-tu acheter des points?", preferredStyle: .alert)

          alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
            NotificationCenter.default.post(name: NotificationName.callPurchasePointButton, object: nil)
          }))
          
          alert.addAction(UIAlertAction(title: "Non merci", style: .cancel, handler: nil))
          viewController.present(alert, animated: true, completion: nil)
        }
      }
  
  
  // show alert when user has enough points, just to confirm he wants to buy
  func showAlertConfirmPurchase(viewController: UIViewController, tutorial: Tutorial, button: UIButton, completion: @escaping () -> ()) {
    let alertView = UIAlertController(title: "Debloque ce tutoriel", message: "Confirme en pressant le button OK", preferredStyle: .alert)
    alertView.addAction(UIAlertAction(title: "OUI", style: .default, handler: { (action) in
      self.points -= tutorial.pricePoints
      self.tutorialUnlocked.append(tutorial.id)
      self.insertItemsToUserDefault()
      
      
      self.savePointsAndTutorialUnlocked(completion: { (error) in
        if let error = error {
          print("error when updating user firebase", error.localizedDescription)
        }
      })
      
      button.backgroundColor = UIColor(red: 120/255, green: 163/255, blue: 165/255, alpha: 1.0)
      button.setTitle("REGARDE LESSON", for: [])
      completion()
    }))
    
    alertView.addAction(UIAlertAction(title: "NON", style: .cancel, handler: { (action) in
      return
    }))
    
    viewController.present(alertView, animated: true, completion: nil)
  }
  
  func savePointsAndTutorialUnlocked(completion: @escaping(Error?) -> Void) {
    let ref = DatabaseReference.users(uid: uid).reference()
    let values = ["points": points, "tutorialUnlocked" : tutorialUnlocked] as [String : Any]
    ref.updateChildValues(values)
  }
  
}


import StoreKit
extension Double {
  func addCurrency(prod: SKProduct) -> String? {
    
    let formatter = NumberFormatter()
    formatter.numberStyle = .currency
    formatter.locale = prod.priceLocale
    let price = formatter.string(from: prod.price)
    if let price = price {
      return price
    }
    return nil
  }
}

