//
//  WebViewController.swift
//  iOSProEcoleAcademie
//
//  Created by SEBASTIEN MONTIBELLER on 5/4/17.
//  Copyright © 2017 SEBASTIEN MONTIBELLER. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController, WKNavigationDelegate {
  
  @IBOutlet weak var defaultView: UIView!
  
  var lesson: Lesson?
  
  lazy var webview: WKWebView = {
    let wv = WKWebView()
    return wv
  }()
  
  
  // MARK: - INITIALIZERS
  override func viewDidLoad() {
    super.viewDidLoad()
    self.defaultView.addSubview(webview)
    webview.anchorToTop(defaultView.topAnchor, left: defaultView.leftAnchor, bottom: defaultView.bottomAnchor, right: defaultView.rightAnchor)
    playUrl()
  }
  
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    title = lesson?.id
    print(lesson?.tutorialUrl)
  }
  
  
  func playUrl() {
    if let fullVideo = lesson {
      webview.navigationDelegate = self
      let url = URL(string: "https://www.youtube.com/embed/\(fullVideo.tutorialUrl)")!
      webview.load(URLRequest(url: url))
      webview.allowsBackForwardNavigationGestures = true
    }
  }
  
  
  @IBAction func retourClicked(_ sender: Any) {
    self.dismiss(animated: true, completion: nil)
  }
  
}
