//
//  DatabaseReference.swift
//  iOSProEcoleAcademie
//
//  Created by SEBASTIEN MONTIBELLER on 20/4/17.
//  Copyright © 2017 SEBASTIEN MONTIBELLER. All rights reserved.
//

import Foundation
import Firebase

enum DatabaseReference {
  
  case root
  case users(uid: String)
  case tutorials
  case tutorialLessons
  case questions
  case documentation
 

    // MARK:- Public
    
    func reference() -> FIRDatabaseReference {
      return rootRef.child(path)
    }
    
    
    private var rootRef: FIRDatabaseReference {
      return FIRDatabase.database().reference()
    }
    
    
    private var path: String {
      switch self {
      case .root:
        return ""
      case .users(let uid):
        return "users/\(uid)"
      case .tutorials:
        return "tutorials"
      case .tutorialLessons:
        return "tutorialLessons"
      case .questions:
        return "questions"
      case .documentation:
        return "documentation"
      }
    }
  }
  
  
  enum StorageReference {
    case root
    case tutorialImages
    case lessonImages
    case moduleImages
    
    func reference() -> FIRStorageReference {
      return baseRef.child(path)
    }
    
    private var baseRef: FIRStorageReference {
      return FIRStorage.storage().reference()
    }
    
    private var path: String {
      switch self {
      case .root:
        return ""
      case .tutorialImages:
        return "tutorialImages"
      case .lessonImages:
        return "lessonImages"
      case .moduleImages:
        return "moduleImages"
      }
    }
    
  }
  
  
  
  

  
  
  

