//
//  AppCell.swift
//  iOSProEcoleAcademie
//
//  Created by SEBASTIEN MONTIBELLER on 28/3/17.
//  Copyright © 2017 SEBASTIEN MONTIBELLER. All rights reserved.
//

import UIKit
import WebKit
import SAMCache

protocol AppCellDelegate: class {
  func purchaseButtonDidTap(tutorial: Tutorial)
  func showAlertNotEnoughPoints()
  func returnViewController() -> UIViewController
  func updateRightBarButton(with  point: Int)
}

class AppCell: UITableViewCell {
  
  // MARK:- IBOutlets
  @IBOutlet weak var appImageView: UIImageView!
  @IBOutlet weak var playButton: UIButton!
  @IBOutlet weak var overviewLabel: UITextView!
  @IBOutlet weak var buyButton: UIButton!
  @IBOutlet weak var numberOfLessonLabel: UILabel!
  
  
  weak var delegate: AppCellDelegate?
  var user: User!
  var cache = SAMCache.shared()
  
  var tutorial: Tutorial? {
    didSet {
      updateUI()
    }
  }
  
  
  override func layoutSubviews() {
    super.layoutSubviews()
    
    buyButton.layer.shadowRadius = 2
    buyButton.layer.shadowOffset = CGSize(width: 2, height: 2)
    buyButton.layer.shadowColor = UIColor.black.cgColor
    buyButton.layer.shadowOpacity = 0.6
    buyButton.layer.cornerRadius = 3
  }
  
  
  func updateUI() {
    
    appImageView.image = nil
    
    guard let tutorial = tutorial else {
      print("Could not find the tutorial")
      return
    }
        
    if user.tutorialUnlocked.contains(tutorial.id) {
      buyButton.backgroundColor = ColorHelper.rgb(red: 120, green: 163, blue: 165, alpha: 1.0).color()
      buyButton.setTitle("REGARDE LECON", for: [])
    } else {
      buyButton.setTitle("DEBLOQUE LECON POUR \(tutorial.pricePoints) POINTS", for: [])
      buyButton.backgroundColor = ColorHelper.rgb(red: 64, green: 44, blue: 38, alpha: 1.0).color()
    }
    
    numberOfLessonLabel.text = "\(tutorial.numberOfEpisode) leçons"
    
    
    // DOWNLOAD IMAGE
    
    let tutorialImageKey = "\(tutorial.id)-tutorialImage"

    if let image = cache?.object(forKey: tutorialImageKey) as? UIImage {
      appImageView.image = image
    } else {
      tutorial.downloadTutorialImage { [weak self] (image, error) in
        if error != nil {
          print(error!)
        } else {
          if let image = image {
            DispatchQueue.main.async {
              self?.appImageView?.image = image
              self?.appImageView?.contentMode = .scaleAspectFit
              self?.appImageView?.clipsToBounds = true
              self?.cache?.setObject(image, forKey: tutorialImageKey)
              
            }
          }
        }
      }
    }
    
    overviewLabel.text = tutorial.tutorialDescription
  }
  
  
  // MARK:- IBActions
  lazy var launchWebview: LaunchWebview = {
    let webview = LaunchWebview()
    webview.delegate = self
    return webview
  }()
  
  
  @IBAction func handlePlayButton(_ sender: Any) {
    launchWebview.showWebview(top: appImageView.topAnchor, left: self.leftAnchor, bottom: appImageView.bottomAnchor, right: self.rightAnchor, tutorial: tutorial)
  }
  
  
  @IBAction func buyButtonPressed(_ sender: Any) {
    
    if let tutorial = tutorial {
      
      if user.tutorialUnlocked.contains(tutorial.id) {
        delegate?.purchaseButtonDidTap(tutorial: tutorial)
        
      } else {
        
        let vc = delegate?.returnViewController()  // get viewController to user below
        
        user.purchaseItemForNumberOf(tutorial: tutorial, viewController: vc!, button: buyButton, completion: {
          self.delegate?.updateRightBarButton(with: self.user.points)
        })        
      }
    }
  }
}








