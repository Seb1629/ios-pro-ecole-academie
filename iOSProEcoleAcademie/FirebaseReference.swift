//
//  FirebaseReference.swift
//  iOSProEcoleAcademie
//
//  Created by SEBASTIEN MONTIBELLER on 20/4/17.
//  Copyright © 2017 SEBASTIEN MONTIBELLER. All rights reserved.
//

import Foundation
import Firebase

enum DatabaseReference {
  
  case root
  case users(uid: String)
  case tutorials
  case tutorialLessons
  case questions
  
  
  func reference() -> FIRDatabaseReference {
    return rootRef.child(path)
  }
  
  
  private var rootRef: FIRDatabaseReference {
    return FIRDatabase.database().reference()
  }
  
  private var path: String {
    switch self {
      
    case .root: return ""
    case .users(uid: let uid): return "user/\(uid)"
    case .tutorials: return "tutorials"
    case .tutorialLessons: return "tutorialLessons"
    case .questions: return "questions"
      
    }
  }
}


enum StorageReference {
  case root
  case tutorialsImages // for articles
  case tutorialLessonsImages
  
  
  func reference() -> FIRStorageReference {
    return baseRef.child(path)
  }
  
  
  private var baseRef: FIRStorageReference {
    return FIRStorage.storage().reference()
  }
  
  private var path: String {
    switch self {
    case .root:
      return ""
    case .tutorialsImages:
      return "tutorialsImages"
    case .tutorialLessonsImages:
      return "tutorialLessonsImages"
    }
  }
}

