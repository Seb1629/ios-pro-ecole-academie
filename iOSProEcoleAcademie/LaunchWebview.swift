//
//  launchWebview.swift
//  iOSProEcoleAcademie
//
//  Created by SEBASTIEN MONTIBELLER on 29/3/17.
//  Copyright © 2017 SEBASTIEN MONTIBELLER. All rights reserved.
//

import UIKit
import WebKit

class LaunchWebview: NSObject, WKNavigationDelegate {
  
  weak var delegate: AppCell? = nil 
  
  let webview: WKWebView = {
    let view = WKWebView()
    view.clipsToBounds = true
    return view
  }()
  
  
  let blackView = UIView()
  let viewContainerForWebview = UIView()
  
  func showWebview(top: NSLayoutYAxisAnchor?, left: NSLayoutXAxisAnchor?, bottom: NSLayoutYAxisAnchor?, right: NSLayoutXAxisAnchor?, tutorial: Tutorial?) {
    
    if let window = UIApplication.shared.keyWindow {
      blackView.backgroundColor = UIColor(white: 0, alpha: 0.9)
      blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleDismiss)))
      
      window.addSubview(blackView)
      blackView.anchorToTop(window.topAnchor, left: window.leftAnchor, bottom: window.bottomAnchor, right: window.rightAnchor)
      blackView.alpha = 0
      
      UIView.animate(withDuration: 0.5, animations: {
      
          self.blackView.alpha = 1
          window.addSubview(self.webview)
          
          self.webview.anchorToTop(top, left: left, bottom: bottom, right: right)
        
        }, completion: { (success: Bool) in

            self.addWebView()
            self.playUrl(tutorial: tutorial)
      })
    }
  }
  
  
  func addWebView() {
    self.webview.navigationDelegate = self
  }
  
  
  func playUrl(tutorial: Tutorial?) {
    if let tutorial = tutorial?.previewUrl {
      let url = URL(string: "https://www.youtube.com/embed/\(tutorial)")!
      webview.load(URLRequest(url: url))
      webview.allowsBackForwardNavigationGestures = true
    }
  }
  
  
  func handleDismiss() {
    UIView.animate(withDuration: 0.5) { 
      self.blackView.alpha = 0
      self.webview.removeFromSuperview()
      AppUtility.lockOrientation(.portrait)
    }
  }
  
}
