//
//  HeaderCell.swift
//  iOSProEcoleAcademie
//
//  Created by SEBASTIEN MONTIBELLER on 28/3/17.
//  Copyright © 2017 SEBASTIEN MONTIBELLER. All rights reserved.
//

import UIKit

class HeaderCell: UITableViewCell {

  @IBOutlet weak var conceptLabel: UILabel!
  @IBOutlet weak var idLabel: UILabel!
  
  var concept: Tutorial? {
    didSet {
      conceptLabel.text = concept?.header
      idLabel.text = concept?.id
    }
  }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.borderWidth = 0.5
        layoutIfNeeded()
        conceptLabel.numberOfLines = 0
      
    }

}
