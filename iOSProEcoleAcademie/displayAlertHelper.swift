//
//  displayAlertHelper.swift
//  iOSProEcoleAcademie
//
//  Created by SEBASTIEN MONTIBELLER on 20/4/17.
//  Copyright © 2017 SEBASTIEN MONTIBELLER. All rights reserved.
//


import UIKit

protocol PresentAlertViewController {}

extension PresentAlertViewController where Self: UIViewController {
  
  func alert(titleAlert: String, textAlert: String, titleBtn: String) {
    let controller = UIAlertController(title: titleAlert, message: textAlert, preferredStyle: .alert)
    let ok = UIAlertAction(title: titleBtn, style: .cancel, handler: nil)
    controller.addAction(ok)
    self.present(controller, animated: true, completion: nil)
  }
  
  
  func alertWithCompletion(titleAlert: String, textAlert: String, titleBtn: String, completion: @escaping () -> ()) {
    let controller = UIAlertController(title: titleAlert, message: textAlert, preferredStyle: .alert)
    let ok = UIAlertAction(title: titleBtn, style: .cancel, handler: nil)
    controller.addAction(ok)
    self.present(controller, animated: true, completion: nil)
    completion()
  }
  
}

