//
//  AlertSheetHelper.swift
//  iOSProEcoleAcademie
//
//  Created by SEBASTIEN MONTIBELLER on 8/4/17.
//  Copyright © 2017 SEBASTIEN MONTIBELLER. All rights reserved.
//

import UIKit
import StoreKit

extension ViewController {
  
  func showAlert(alertTitle: String, alertMessage: String, products: [SKProduct]) {
    let alert = UIAlertController(title: alertTitle, message: alertMessage , preferredStyle: .alert)
    
    alert.addAction(UIAlertAction(title: "100 Points pour \(checkPriceForItem(coins: coins100))", style: .default, handler: { (action) in
      SKPaymentQueue.default().add(self)
      let payment = SKPayment(product: self.retrieveSKProduct(coins: self.coins100))
      SKPaymentQueue.default().add(payment)
    }))
    
    alert.addAction(UIAlertAction(title: "200 Points pour \(checkPriceForItem(coins: coins200))", style: .default, handler: { (action) in
      SKPaymentQueue.default().add(self)
      let payment = SKPayment(product: self.retrieveSKProduct(coins: self.coins200))
      SKPaymentQueue.default().add(payment)
    }))
    
    alert.addAction(UIAlertAction(title: "500 Points pour \(checkPriceForItem(coins: coins500))", style: .default, handler: { (action) in
      SKPaymentQueue.default().add(self)
      let payment = SKPayment(product: self.retrieveSKProduct(coins: self.coins500))
      SKPaymentQueue.default().add(payment)
    }))
    
    alert.addAction(UIAlertAction(title: "1000 Points pour \(checkPriceForItem(coins: coins1000))", style: .default, handler: { (action) in
      SKPaymentQueue.default().add(self)
      let payment = SKPayment(product: self.retrieveSKProduct(coins: self.coins1000))
      SKPaymentQueue.default().add(payment)
    }))
    
    alert.addAction(UIAlertAction(title: "Non merci", style: .cancel, handler: nil))
    self.present(alert, animated: true, completion: nil)
  }
  
  
  func retrieveSKProduct(coins: ModelInAppPurchase) -> SKProduct {
    for product in products {
      if product.productIdentifier == coins.productIdentifier {
        return product
      }
    }
    return SKProduct()
  }
  
  
  func checkPriceForItem(coins: ModelInAppPurchase) -> String {
    for product in products {
      if product.productIdentifier == coins.productIdentifier {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = product.priceLocale
        let price = formatter.string(from: product.price)
        if let price = price {
          return price
        }
      }
    }
     return ""
  }
  
}

