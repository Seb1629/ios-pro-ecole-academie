//
//  ViewController.swift
//  iOSProEcoleAcademie
//
//  Created by SEBASTIEN MONTIBELLER on 28/3/17.
//  Copyright © 2017 SEBASTIEN MONTIBELLER. All rights reserved.
//

import UIKit
import Social
import MessageUI
import CloudKit
import StoreKit
import SwiftSpinner


struct StoryBoardSegue {
  static let cellId = "cellId"
  static let goToLearnVC = "goToLearnVC"
  static let goToAdmin = "goToAdmin"
}


class ViewController: UIViewController, MFMailComposeViewControllerDelegate {
  
  struct Storyboard {
    static let cellId = "cellId"
    static let headerCellId = "headerCellId"
    static let cellHeaderHeight: CGFloat = 40
    static let appCellHeight: CGFloat = 500
    static let GoToQCM = "GoToQCM"
  }
  
  
  var tutorials = [Tutorial]()
  var viewController: UIViewController!
  var userUID: String!
  var userPoints: Int!
  var user: User!
  
  // storeKit
  var products = [SKProduct]()
  var coins100: ModelInAppPurchase!
  var coins200: ModelInAppPurchase!
  var coins500: ModelInAppPurchase!
  var coins1000: ModelInAppPurchase!

  
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var pointsButton: UIButton!
  @IBOutlet weak var segmentedControl: UISegmentedControl!
  @IBOutlet weak var adminBtn: UIButton!


  // MARK: - Initializers
  override func viewDidLoad() {
    super.viewDidLoad()
    setupCoinsProduct()
    loadUserAndPoint()
    fetchEpisodes()
    
    let titleDict = [NSForegroundColorAttributeName: UIColor.white]
    self.navigationController?.navigationBar.titleTextAttributes = titleDict
    configCollectionView()
    requestProducts()
  }

  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    showAdminAccess()
    AppUtility.lockOrientation(.portrait)
    NotificationCenter.default.addObserver(self, selector: #selector(showPurchaseOptions), name: NotificationName.callPurchasePointButton, object: nil)
  }
  
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    
    // Don't forget to reset when view is being removed
    AppUtility.lockOrientation(.all)
    NotificationCenter.default.removeObserver(self, name: NotificationName.callPurchasePointButton, object: nil)
  }

  
  // MARK: - Functions
  
  @IBAction func adminPressed(_ sender: Any) {
    performSegue(withIdentifier: StoryBoardSegue.goToAdmin, sender: nil)
  }
  
  
  func fetchEpisodes() {
    SwiftSpinner.show(duration: 5, title: "Preparing lessons")
    Tutorial.fetchTutorialsFromDatabase() { [weak self] (tutorial) in
      
        guard let tutorial = tutorial, let strongSelf = self else {
          SwiftSpinner.hide()
          return
        }

        if !strongSelf.tutorials.contains(tutorial) {
          
          DispatchQueue.main.async {
            SwiftSpinner.hide()

            strongSelf.tutorials.append(tutorial)
            strongSelf.tableView.reloadData()
        }
      }
    }
  }
  
  
  func showPurchaseOptions() {
    self.showAlert(alertTitle: "Je veux debloquer de nouvelle videos!", alertMessage: "Choisis parmis nos selections de points pour debloquer plus de materiels", products: products)
  }
  
  
  func loadUserAndPoint() {
    
    if let userData = UserDefaults.standard.object(forKey: UserDefaultsKeys.userObject) as? Data {
      let user = NSKeyedUnarchiver.unarchiveObject(with: userData)
      self.user = user as! User
      self.pointsButton.setTitle("\(self.user.points) POINTS", for: [])
      print("retrieved this guy: \(self.user.emailAddress)")
    }
  }
  
  
  @IBAction func addPointsTapped(_ sender: Any) {
    self.showAlert(alertTitle: "Je veux debloquer de nouvelle videos!", alertMessage: "Choisis parmis nos selections de points pour debloquer plus de materiels", products: products)
  }
  
  
  @IBAction func segmentedControlTapped(_ sender: Any) {
    
    if segmentedControl.selectedSegmentIndex == 0 {
      tutorials.removeAll()
      fetchEpisodes()
      tableView.reloadData()
    } else if segmentedControl.selectedSegmentIndex == 1 {
      tutorials.removeAll()
      fetchEpisodes()
      tableView.reloadData()
    }
  }
  
  
  @IBAction func requestCourseIdea(_ sender: Any) {
  let mailComposeViewController = configureMailComposeViewController()
    if MFMailComposeViewController.canSendMail() {
      self.present(mailComposeViewController, animated: true, completion: nil)
      
    } else {
      showSendMailErrorAlert()
    }
  }
  
  
  func setupCoinsProduct() {
    coins100 = ModelInAppPurchase(name: "coinsOneHundred", productIdentifier: "com.ibuildyourmobileapp.iOSProEcoleAcademie100points", price: 0.99, points: 100)
    coins200 = ModelInAppPurchase(name: "coinstwoHundred", productIdentifier: "com.ibuildyourmobileapp.iOSProEcoleAcademie_200Points", price: 1.98, points: 200)
    coins500 = ModelInAppPurchase(name: "coinsfiveHundred", productIdentifier: "com.ibuildyourmobileapp.iOSProEcoleAcademie500points", price: 3.99, points: 500)
    coins1000 = ModelInAppPurchase(name: "coinsOneThousand", productIdentifier: "com.ibuildyourmobileapp.iOSProEcoleAcademie1000points", price: 7.99, points: 1000)
  }
  

   func configureMailComposeViewController() -> MFMailComposeViewController {
    
    let mailComposer = MFMailComposeViewController()
    mailComposer.mailComposeDelegate = self
    mailComposer.setToRecipients(["montycoder@gmail.com"])
    mailComposer.setSubject("Nouvelle lecons")
    mailComposer.setMessageBody("Bonjour Sebastien, pourrais tu creer un cours a propos de ...", isHTML: false)
    
    return mailComposer
  }
  
  
   func showSendMailErrorAlert() {
    let sendMailErrorAlert = UIAlertController(title: "Could not send email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", preferredStyle: .alert)
    let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
    sendMailErrorAlert.addAction(okAction)
    present(sendMailErrorAlert, animated: true, completion: nil)
  }
  
  
  func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
    controller.dismiss(animated: true, completion: nil)
  }
  
  
  @IBAction func shareAppPressed(_ sender: Any) {
   
    // FIXME: replace later with the proper link once the app is available
    
    let textToShare = "Swift is awesome! Decouvre ce cours iOS pour Francais!"
    
    if let appleLink = URL(string: "https://itunes.apple.com/us/app/app-name/id12345678?ls=1&mt=8") {
      let activityController = UIActivityViewController(activityItems: [textToShare, appleLink], applicationActivities: [])
      present(activityController, animated: true, completion: nil)
    }
  }
  

  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
  
      if segue.identifier == StoryBoardSegue.goToLearnVC {
      if let destination = segue.destination as? CourseViewController {
        if let sender = sender as? Tutorial {
          destination.tutorial = sender
          destination.currentUser = user
        }
      }
    }
  }
}



// PERSONAL ACCESS TO ADMIN SCREEN
extension ViewController {
  
  func showAdminAccess() {
    if user.emailAddress == "seb.montibeller@gmail.com" {
      adminBtn.isEnabled = true
      adminBtn.isHidden = false
    } else {
      adminBtn.isEnabled = false
      adminBtn.isHidden = true
    }
  }
  
}


extension ViewController: AppCellDelegate {
  
  func purchaseButtonDidTap(tutorial: Tutorial) {
    self.performSegue(withIdentifier: StoryBoardSegue.goToLearnVC, sender: tutorial)
//    self.showActionSheet(tutorial: tutorial)
  }
  
  func showAlertNotEnoughPoints() {
    self.showAlert(alertTitle: "Ooops tu n'as pas assez de points", alertMessage: "Veux-tu acheter des points?", products: products)
  }
  
  func returnViewController() -> UIViewController {
    return self
  }
  
  func updateRightBarButton(with point: Int) {
    DispatchQueue.main.async {
      self.pointsButton.setTitle("\(point) POINTS", for: [])
    }
  }
}




