//
//  ViewController+StoreKit.swift
//  iOSProEcoleAcademie
//
//  Created by SEBASTIEN MONTIBELLER on 7/4/17.
//  Copyright © 2017 SEBASTIEN MONTIBELLER. All rights reserved.
//

import Foundation
import StoreKit

extension ViewController: SKProductsRequestDelegate {
  
  func requestProducts() {
    let ids: Set<String> = ["com.ibuildyourmobileapp.iOSProEcoleAcademie100points", "com.ibuildyourmobileapp.iOSProEcoleAcademie_200Points", "com.ibuildyourmobileapp.iOSProEcoleAcademie500points", "com.ibuildyourmobileapp.iOSProEcoleAcademie1000points"]
    let productRequest = SKProductsRequest(productIdentifiers: ids)
    productRequest.delegate = self
    productRequest.start()
  }
  
  func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
    
    // put alert for invalid product
    self.products = response.products
  }
}


extension ViewController: SKPaymentTransactionObserver {
  func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
    for transaction in transactions {
      switch transaction.transactionState {
      case .purchased:
        addPoints(to: user, productIdentifier: transaction.payment.productIdentifier)
        SKPaymentQueue.default().finishTransaction(transaction)
      case .purchasing:
        print("purchasing")
      case .failed:
        SKPaymentQueue.default().finishTransaction(transaction)
      case .restored:
        SKPaymentQueue.default().finishTransaction(transaction)
      case .deferred:
        print("deferred")
      }
    }
  }
  
  
  func addPoints(to user: User, productIdentifier: String) {
     let coinsArray = [coins100, coins200, coins500, coins1000]
    
    for coin in coinsArray {
      if coin?.productIdentifier == productIdentifier {
        user.points += coin?.points ?? 0
        pointsButton.setTitle("\(user.points) POINTS", for: [])
        user.insertItemsToUserDefault()
        
        user.savePointsAndTutorialUnlocked(completion: { (error) in
          if let error = error {
            print(error.localizedDescription)
          }
        })
      }
    }
    }
  
  }
  


