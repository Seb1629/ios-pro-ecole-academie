//
//  CommonQuestions.swift
//  iOSProEcoleAcademie
//
//  Created by SEBASTIEN MONTIBELLER on 5/4/17.
//  Copyright © 2017 SEBASTIEN MONTIBELLER. All rights reserved.
//

// FIXME:- CHANGE TO FIREBASE DATABASE

import Foundation
import CloudKit

class CommonQuestion {
  
  let question: String
  let response: String
  let reference: String
  
  
  init(dict: CKRecord) {
    
    question = dict["question"] as? String ?? ""
    response = dict["response"] as? String ?? ""
    reference = dict["reference"] as? String ?? ""
    
  }
  
  
  class func fetchQuestions(reference: String, completion: @escaping (_ questionPerTopic: CommonQuestion?) -> Void) {
    
    let publicData = CKContainer.default().publicCloudDatabase
    let predicate = NSPredicate(format: "%K == %@", "reference", "TestA")
    let query = CKQuery(recordType: "Questions", predicate: predicate)
    publicData.perform(query, inZoneWith: nil) { (result: [CKRecord]?, error) in
      if let record = result {
        for rec in record {
          let questionFetched = CommonQuestion(dict: rec)
          completion(questionFetched)
        }
      }
    }
  }


}
