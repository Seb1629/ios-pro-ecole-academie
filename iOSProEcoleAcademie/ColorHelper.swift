//
//  ColorHelper.swift
//  iOSProEcoleAcademie
//
//  Created by SEBASTIEN MONTIBELLER on 18/4/17.
//  Copyright © 2017 SEBASTIEN MONTIBELLER. All rights reserved.
//

import UIKit

enum ColorHelper {
  
  case rgb(red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat)
  
  func color() -> UIColor {
    switch self {
    case .rgb(red: let red, green: let green, blue: let blue, let alpha):
      return UIColor(red: red / 255, green: green / 255, blue: blue / 255, alpha: alpha)
    }
  }
  
  
}
