//
//  QCMViewController.swift
//  iOSProEcoleAcademie
//
//  Created by SEBASTIEN MONTIBELLER on 5/4/17.
//  Copyright © 2017 SEBASTIEN MONTIBELLER. All rights reserved.
//

import UIKit

class QCMViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

  struct TableView {
    static let reuseIdentifier = "reuseIdentifier"
    static let headerCell = "headerCell"
    static let cellHeight: CGFloat = 80
    static let goToAnswerVC = "goToAnswerVC"
  }
  
  @IBOutlet weak var tableView: UITableView!
  
  let imageView: UIImageView = {
    let iv = UIImageView()
    iv.contentMode = .scaleAspectFill
    let image = #imageLiteral(resourceName: "travelApp")
    iv.image = image
    return iv
  }()

  var questions = [CommonQuestion]()
  var lesson: Lesson?
  
  var emptyView: EmptyView?
  
    override func viewDidLoad() {
        super.viewDidLoad()
      tableView.delegate = self
      tableView.dataSource = self
      
      fetchQuestions {
         self.displayWhiteViewIfNoQuestions()
      }

      tableView.estimatedRowHeight = TableView.cellHeight
      tableView.rowHeight = UITableViewAutomaticDimension
      tableView.backgroundView = imageView
        addBlur()
    
    }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
          AppUtility.lockOrientation(.portrait)
  }
  
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
     AppUtility.lockOrientation(.all)
  }
  
  func addBlur() {

    let blurEffect = UIBlurEffect(style: .extraLight)
    let blurredEffectView = UIVisualEffectView(effect: blurEffect)
    blurredEffectView.frame = imageView.frame
    imageView.addSubview(blurredEffectView)
    
  }
  
  private func displayWhiteViewIfNoQuestions() {
    if questions.count == 0 {
      emptyView = EmptyView(view: self.view)
      emptyView?.displayView()
    } else {
      
      if emptyView != nil {
        emptyView = nil
      }
    }
  }
  
  
  func fetchQuestions(completion: (_ action: ()) -> Void) {
    
    guard let lesson = lesson else { return }
    
    CommonQuestion.fetchQuestions(reference: lesson.id) { [weak self] (question) in
      if let question = question {
        DispatchQueue.main.async {
          self?.questions.append(question)
          self?.tableView.reloadData()
        }
      }
    }
  }
  
  
  @IBAction func RetourTapped(_ sender: Any) {
    dismiss(animated: true, completion: nil)
  }

}

extension QCMViewController {
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return questions.count
  }
  
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if let cell = tableView.dequeueReusableCell(withIdentifier: TableView.reuseIdentifier, for: indexPath) as? QCMViewCell {
      
      cell.question = questions[indexPath.row]
      
      return cell
    } else {
      return UITableViewCell()
    }
  }
  

  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let cell = tableView.dequeueReusableCell(withIdentifier: TableView.headerCell) as! HeadCell
   cell.header = lesson
    return cell
  }
  
  
  func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
    return 120
  }
  
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableViewAutomaticDimension
  }
  
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let question = questions[indexPath.row]
    performSegue(withIdentifier: TableView.goToAnswerVC, sender: question)
  }
  
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == TableView.goToAnswerVC {
      if let destination = segue.destination as? AnswerViewController {
        
        if let sender = sender as? CommonQuestion {
          destination.question = sender
        }
      }
    }
  }
  
}


class HeadCell: UITableViewCell {
  
  @IBOutlet weak var headerLabel: UILabel!
  
  var header: Lesson! {
    didSet {
        headerLabel.text = header.id
    }
  }
}


