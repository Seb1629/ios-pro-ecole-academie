//
//  FIRImage.swift
//  iOSProEcoleAcademie
//
//  Created by SEBASTIEN MONTIBELLER on 20/4/17.
//  Copyright © 2017 SEBASTIEN MONTIBELLER. All rights reserved.
//
//

import Foundation
import Firebase

class FIRImage {
  
  var image: UIImage
  var downloadURL: URL?
  var downloadLInk: String!
  var ref: FIRStorageReference!
  
  init(image: UIImage) {
    self.image = image
  }
  
}


extension FIRImage {
  
  func saveTutorialImage(_ userUID: String,_ completion: @escaping(Error?) -> Void) {
    let resizedImage = image.resized()
    let imageData = UIImageJPEGRepresentation(resizedImage, 0.3)
    
    ref = StorageReference.tutorialImages.reference().child(userUID)
    downloadLInk = ref.description
    
    ref.put(imageData!, metadata: nil) { (metaData, error) in
      completion(error)
    }
  }
  
  
  func saveLessonImage(_ userUID: String, _ leconForImage: Int ,_ completion: @escaping(Error?) -> Void) {
    let resizedImage = image.resized()
    let imageData = UIImageJPEGRepresentation(resizedImage, 0.3)
    
    ref = StorageReference.lessonImages.reference().child("\(userUID)_\(leconForImage)")
    downloadLInk = ref.description
    
    ref.put(imageData!, metadata: nil) { (metadata, error) in
      completion(error)
    }
  }
  
}


extension FIRImage {
  class func downloadTutorialImage(_ uid: String, completion: @escaping (UIImage?, Error?) -> Void) {
    StorageReference.tutorialImages.reference().child(uid).data(withMaxSize: 1 * 1024 * 1024) { (imageData, error) in
      if error == nil && imageData != nil {
        let image = UIImage(data: imageData!)
        completion(image, error)
        
      } else {
        completion(nil, error)
      }
    }
  }
  
  
  class func downloadLessonImage(uid: String, lessonForImage: Int, completion: @escaping(UIImage?, Error?) -> Void) {
    StorageReference.lessonImages.reference().child("\(uid)_\(lessonForImage).png").data(withMaxSize: 1 * 1024 * 1024) { (imageData, error) in
      if error == nil && imageData != nil {
        let image = UIImage(data: imageData!)
        completion(image, error)
      } else {
        completion(nil, error)
      }
    }
  }
  
}


private extension UIImage {
  func resized() -> UIImage {
    let height: CGFloat = 800.0
    let ratio = self.size.width / self.size.height
    let width = height * ratio
    
    let newSize = CGSize(width: width, height: height)
    let newRectangle = CGRect(x: 0, y: 0, width: width, height: height)
    
    UIGraphicsBeginImageContext(newSize)
    self.draw(in: newRectangle)
    
    let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return resizedImage!
  }
}
