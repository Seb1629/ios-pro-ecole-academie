//
//  AddDollarsSignToString.swift
//  iOSProEcoleAcademie
//
//  Created by SEBASTIEN MONTIBELLER on 29/3/17.
//  Copyright © 2017 SEBASTIEN MONTIBELLER. All rights reserved.
//

import Foundation

extension Double {
  var dollarString: String {
    return String(format: "$%.02f", self)
  }
}
