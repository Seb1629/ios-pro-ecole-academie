//
//  HomeViewController.swift
//  iOSProEcoleAcademie
//
//  Created by SEBASTIEN MONTIBELLER on 8/4/17.
//  Copyright © 2017 SEBASTIEN MONTIBELLER. All rights reserved.
//

import UIKit
import CloudKit
import Firebase
import FBSDKLoginKit


class HomeViewController: UIViewController {
  
  @IBOutlet weak var startButton: UIButton!
  
  private struct StoryBoardSegue {
    static let LoggedInSegue = "LoggedInSegue"
  }
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    startButton.imageView?.contentMode = .scaleAspectFill
  }
  

  func retrieveUserAndLogInIfExist() {
    
    if let data = UserDefaults.standard.object(forKey: UserDefaultsKeys.userObject) as? Data {
      let user = NSKeyedUnarchiver.unarchiveObject(with: data)
      performSegue(withIdentifier: StoryBoardSegue.LoggedInSegue, sender: user)
    }
  }
  
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    AppUtility.lockOrientation(.portrait)
    retrieveUserAndLogInIfExist()
  }
  
  
  // MARK: - IBActions
  
  @IBAction func startButtonTapped(_ sender: Any) {
    
    let fbLoginManager = FBSDKLoginManager()
    fbLoginManager.logIn(withReadPermissions: ["public_profile", "email"], from: self) { [weak self] (result, error) in
      if let error = error {
        print("Failed to login: \(error.localizedDescription)")
        return
      }
      
      guard let accessToken = FBSDKAccessToken.current() else {
        print("Failed to get access token")
        return
      }
      
      let credential = FIRFacebookAuthProvider.credential(withAccessToken: accessToken.tokenString)
      
      // Perform login by calling Firebase APIs
      self?.firebaseAuth(credential: credential)
    }
  }
  
  
  func firebaseAuth(credential: FIRAuthCredential) {
    
    FIRAuth.auth()?.signIn(with: credential, completion: { [weak self] (user, error) in
      if let error = error {
        print("Login error: \(error.localizedDescription)")
        let alertController = UIAlertController(title: "Login Error", message: error.localizedDescription, preferredStyle: .alert)
        let okayAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alertController.addAction(okayAction)
        self?.present(alertController, animated: true, completion: nil)
        
        return
      }
      
      // CREATE USER AND SAVE TO USERDEFAULT
      let user = User(uid: user?.uid ?? "", emailAddress: user?.email ?? "", name: user?.displayName ?? "")
      user.insertItemsToUserDefault()
      print("user: \(user.name) saved to Default")
      
      // SEND TO NEXT SCREEN
      self?.performSegue(withIdentifier: StoryBoardSegue.LoggedInSegue, sender: user)
      
      // SAVE TO FIREBASE
      user.save(completion: { (error) in
        if error != nil {
          print(error!.localizedDescription)
        }
      })
    })
  }
  
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == StoryBoardSegue.LoggedInSegue {
      if let destination = segue.destination as? ViewController {
        if let sender = sender as? User {
          destination.user = sender
        }
      }
    }
  }
  
}
