//
//  Tutorial.swift
//  iOSProEcoleAcademie
//
//  Created by SEBASTIEN MONTIBELLER on 28/3/17.
//  Copyright © 2017 SEBASTIEN MONTIBELLER. All rights reserved.
//

import UIKit
import CloudKit
import Firebase

class Tutorial {
  
  let id: String
  let header: String
  var tutorialImage: UIImage?
  let tutorialDescription: String
  let pricePoints: Int
  let previewUrl: String
  let numberOfEpisode: Int

  
  init(id: String, header: String, tutorialDescription: String, pricePoints: Int, previewUrl: String, numberOfEpisode: Int) {
    self.id = id
    self.header = header
    self.tutorialDescription = tutorialDescription
    self.pricePoints = pricePoints
    self.previewUrl = previewUrl
    self.numberOfEpisode = numberOfEpisode
  }
  
  
  init(dictionary: [String: Any]) {
    self.id = dictionary["id"] as? String ?? ""
    self.header = dictionary["header"] as? String ?? ""
    self.tutorialDescription = dictionary["tutorialDescription"] as? String ?? ""
    self.pricePoints = dictionary["pricePoints"] as? Int ?? 0
    self.previewUrl = dictionary["previewUrl"] as? String ?? ""
    self.tutorialImage = dictionary["tutorialImage"] as? UIImage
    self.numberOfEpisode = dictionary["numberOfEpisode"] as? Int ?? 0
  }
  
  
  func saveTutorial(completion: @escaping (Error?) -> Void) {
    let ref = DatabaseReference.tutorials.reference().child(id) 
    ref.setValue(toDictionary())
    
  }
  
  
  func toDictionary() -> [String: Any] {
    return [
      "id" : id,
      "header" : header,
      "tutorialDescription" : tutorialDescription,
      "pricePoints" : pricePoints,
      "previewUrl" : previewUrl,
      "numberOfEpisode" : numberOfEpisode,
      
    ]
  }
  
}

extension Tutorial {
  
  class func fetchTutorialsFromDatabase(completion: @escaping (_ tutorial: Tutorial?) -> Void) {
    
    DatabaseReference.tutorials.reference().observe(.childAdded, with: { (snapshot) in
      if let dict = snapshot.value as? [String: Any] {
        
        let tutorial = Tutorial(dictionary: dict)
        print(dict)
        completion(tutorial)
      }
    }, withCancel: nil)
    
  }
  

  func downloadTutorialImage(completion: @escaping (UIImage?, NSError?) -> Void) {
    FIRImage.downloadTutorialImage("\(id).png") { (image, error) in
      self.tutorialImage = image
      completion(image, error as NSError?)
    }
  }

  
}

extension Tutorial: Equatable {}
  
  func ==(lhs: Tutorial, rhs: Tutorial) -> Bool {
    return lhs.id == rhs.id
  }

























